# vendor

This repository is a test fixture for gomodproxy. It contains a vendor
directory that is stripped when Go module is packaged into a ZIP archive.
